<?php
namespace CodeWorking\CSV\Tests;

use CodeWorking\CSV\CsvReader;
use PHPUnit\Framework\TestCase;

/**
 *
 * @author Joachim Beig
 *        
 */
class CsvReaderTest extends TestCase
{

    public function testConstruct()
    {
        $this->assertInstanceOf(CsvReader::class, new CsvReader('test.csv', [
            'mode' => 'w+'
        ]));
    }
}