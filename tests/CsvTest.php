<?php
namespace CodeWorking\CSV\Tests;

use CodeWorking\CSV\Csv;
use PHPUnit\Framework\TestCase;

class CsvTest extends TestCase
{

    /**
     * CSV File name to read from.
     *
     * @var string
     */
    protected $testFileRead = 'test_reader.csv';

    /**
     * CSV File name to write to.
     *
     * @var string
     */
    protected $testFileWrite = 'test_writer.csv';

    /**
     * Contents of the test CSV file.
     *
     * @var array
     */
    protected $testData = [];

    /**
     * Header of the test CSV file.
     *
     * @var array
     */
    protected $testDataHeader = [];

    /**
     * The raw text file header of the CSV file.
     *
     * @var string
     */
    protected $rawHeader = "";

    /**
     * The raw text file content fo the CSV file.
     *
     * @var string
     */
    protected $rawContent = "";

    protected function setUp()
    {
        $this->testData = [
            [
                'line1',
                'value1',
                1,
                'valueA'
            ],
            [
                'line2',
                'value2',
                2,
                'valueB'
            ],
            [
                'line3',
                'value3',
                3,
                'valueC'
            ]
        ];
        
        $this->testDataHeader = [
            'Line',
            'Number Value',
            'Number',
            'Text Value'
        ];
        
        $this->rawHeader = "Line,\"Number Value\",Number,\"Text Value\"\n";
        
        $this->rawContent = "line1,value1,1,valueA\nline2,value2,2,valueB\nline3,value3,3,valueC\n";
        
        $this->createTestFiles();
    }

    public function tearDown()
    {
        if (is_file($this->testFileRead)) {
            unlink($this->testFileRead);
        }
        if (is_file($this->testFileWrite)) {
            unlink($this->testFileWrite);
        }
    }

    public function testConstructAndClose()
    {
        $csv = new Csv($this->testFileRead);
        $this->assertInstanceOf(Csv::class, $csv);
        $csv->close();
    }

    public function testLoadHeader()
    {
        $csv = new Csv($this->testFileRead, [
            'mode' => 'r'
        ]);
        $header = $csv->loadHeader();
        $this->assertEquals($header, $this->testDataHeader);
        $csv->close();
    }

    public function testSetHeader()
    {
        $csv = new Csv($this->testFileRead, [
            'mode' => 'r'
        ]);
        
        $newHeader = [
            'Column1',
            'Column2',
            'Column3',
            'Column4'
        ];
        
        $csv->setHeader($newHeader);
        $firstRecord = $csv->nextRecord();
        $this->assertEquals($newHeader, array_keys($firstRecord));
        $csv->close();
    }

    public function testGetRecordIndex()
    {
        $csv = new Csv($this->testFileRead, [
            'mode' => 'r'
        ]);
        $csv->nextRecord();
        $this->assertEquals($csv->getRecordIndex(), 1);
    }

    public function testNextRecord()
    {
        $csv = new Csv($this->testFileRead, [
            'mode' => 'r'
        ]);
        $csv->loadHeader();
        $record = $csv->nextRecord();
        $this->assertEquals(array_combine($this->testDataHeader, $this->testData[0]), $record);
        $csv->close();
    }

    public function testWriteHeader()
    {
        $csv = new Csv($this->testFileWrite, [
            'mode' => 'w'
        ]);
        $csv->writeHeader($this->testDataHeader);
        $csv->close();
        $this->assertEquals($this->rawHeader, file_get_contents($this->testFileWrite));
    }

    public function testWriteRecord()
    {
        $csv = new Csv($this->testFileWrite, [
            'mode' => 'w'
        ]);
        foreach ($this->testData as $record) {
            $csv->writeRecord($record);
        }
        $csv->close();
        $this->assertEquals($this->rawContent, file_get_contents($this->testFileWrite));
    }

    public function testWriteRecords()
    {
        $csv = new Csv($this->testFileWrite, [
            'mode' => 'w'
        ]);
        $csv->writeRecords($this->testData);
        $csv->close();
        $this->assertEquals($this->rawContent, file_get_contents($this->testFileWrite));
    }

    public function testClose()
    {
        $csv = new Csv($this->testFileRead, [
            'mode' => 'r'
        ]);
        $csv->close();
        $this->assertFalse(is_resource($csv->getHandler()));
        
        $csv = new Csv($this->testFileRead, [
            'mode' => 'r'
        ]);
        $csv->close(true);
        $this->assertFalse(file_exists($this->testFileRead));
        
        $this->createTestFiles();
    }

    public function testGetStreamContent()
    {
        $csv = new Csv('php://temp', [
            'mode' => 'w'
        ]);
        $csv->writeHeader($this->testDataHeader);
        $csv->writeRecords($this->testData);
        $result = $csv->getStreamContent();
        $csv->close();
        $this->assertEquals($result, $this->rawHeader . $this->rawContent);
    }

    public function testGetHandler()
    {
        $csv = new Csv($this->testFileRead, [
            'mode' => 'r'
        ]);
        $handler = $csv->getHandler();
        $this->assertTrue(is_resource($handler));
        $csv->close();
    }

    private function createTestFiles()
    {
        file_put_contents($this->testFileRead, $this->rawHeader . $this->rawContent);
    }
}