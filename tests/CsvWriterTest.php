<?php
namespace CodeWorking\CSV\Tests;

use CodeWorking\CSV\CsvWriter;
use PHPUnit\Framework\TestCase;

/**
 *
 * @author Joachim Beig
 *        
 */
class CsvWriterTest extends TestCase
{

    public function testConstruct()
    {
        $this->assertInstanceOf(CsvWriter::class, new CsvWriter('test.csv'));
    }
}