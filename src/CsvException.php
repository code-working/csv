<?php
namespace CodeWorking\CSV;

/**
 * CSV Exception.
 *
 * This Exception is thrown when there are read or write errors.
 *
 * @author Joachim Beig
 *        
 */
class CsvException extends \RuntimeException
{
}