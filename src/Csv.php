<?php
namespace CodeWorking\CSV;

/**
 * A wrapper class to read and write CSV files very comfortable.
 *
 * @author Joachim Beig
 *        
 */
class Csv
{

    /**
     * The file name of the CSV file.
     *
     * @var string
     */
    private $file = null;

    /**
     * The resource file pointer to read/write to from.
     *
     * @var resource
     */
    private $csv = null;

    /**
     * Contains the header of the CSV data.
     *
     * @var string
     */
    private $header = null;

    /**
     * Contains the index of the current CSV record.
     *
     * @var int
     */
    private $recordIndex = 0;

    /**
     * Configuration.
     *
     * @var array
     */
    private $config = null;

    /**
     * Default configuration that can be overwritten.
     *
     * @var array
     */
    private $_config = [
        'mode' => 'r+',
        'header' => true,
        'length' => 0,
        'delimiter' => ',',
        'enclosure' => '"',
        'escape' => '\\'
    ];

    public function __construct($file, $config = [])
    {
        $this->config = $config + $this->_config;

        $this->file = $file;

        // Create the file resource pointer
        $this->csv = fopen($this->file, $this->config['mode']);

        // Check if the file can be read
        if (! $this->csv) {
            throw new CsvException('Could not open file "' . $file . '".');
        }
    }

    /**
     * Return the file of the CSV instance.
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Load the header from the CSV file.
     *
     * @return array|false
     */
    public function loadHeader()
    {
        if ($this->config['header']) {
            $this->header = $this->nextRecord();
            $this->recordIndex --;
        }

        return $this->header;
    }

    /**
     * Set the header fields of the CSV file.
     *
     * @param array $header
     */
    public function setHeader($header)
    {
        $this->header = $header;
        return $this;
    }

    /**
     * Return the index of the current record.
     *
     * @return int
     */
    public function getRecordIndex()
    {
        return $this->recordIndex;
    }

    /**
     * Get the next record of the CSV file.
     *
     * If a header is set, an associative array is returned with the header fields as keys.
     *
     * @return array|false The next record is returned or FALSE if the end of the file is reached
     */
    public function nextRecord()
    {
        $record = fgetcsv($this->csv, $this->config['length'], $this->config['delimiter'], $this->config['enclosure'], $this->config['escape']);

        if ($this->header !== null && $record !== false) {
            if (count($record) != count($this->header)) {
                throw new CsvException("Record column count differs from header in record " . $this->getRecordIndex() . ".\nHeader: " . print_r($this->header, true) . "\nRecord: " . print_r($record, true));
            }
            $record = array_combine($this->header, $record);
        }

        $this->recordIndex ++;

        return $record;
    }

    /**
     * Write and set the header.
     *
     * @param array $header
     * @return \CodeWorking\CSV\Csv
     */
    public function writeHeader(array $header)
    {
        $this->header = $header;
        $this->writeRecord($this->header);
        return $this;
    }

    /**
     * Write an record to the CSV file.
     *
     * @param string|array $record
     * @throws CsvException
     * @return \CodeWorking\CSV\Csv
     */
    public function writeRecord($record)
    {
        // Convert record to array
        if (! is_array($record)) {
            $record = [
                $record
            ];
        }

        // Check with header count
        if ($this->header !== null && count($record) != count($this->header)) {
            throw new CsvException("Record column count differs from header in record " . $this->getRecordIndex() . ".\nHeader: " . print_r($this->header, true) . "\nRecord: " . print_r($record, true));
        }

        // Write the record
        fputcsv($this->csv, $record, $this->config['delimiter'], $this->config['enclosure'], $this->config['escape']);

        $this->recordIndex ++;

        return $this;
    }

    /**
     * Write a batch of records to the CSV file.
     *
     * @param array $records
     */
    public function writeRecords(array &$records)
    {
        foreach ($records as $record) {
            $this->writeRecord($record);
        }
    }

    /**
     * Close the file resource pointer.
     *
     * @param bool $unlink
     *            If set to true, the underlying file is deleted.
     */
    public function close($unlink = false)
    {
        fclose($this->csv);

        if ($unlink) {
            unlink($this->file);
        }
    }

    /**
     * Read and return contents of the stream resource.
     * This only works if the resource file is a stream.
     *
     * @return string
     */
    public function getStreamContent()
    {
        // Rewind the stream handle
        fseek($this->csv, 0);

        // Return stream content
        return stream_get_contents($this->csv);
    }

    /**
     * Access the resource handler.
     *
     * @return resource
     */
    public function getHandler()
    {
        return $this->csv;
    }
}