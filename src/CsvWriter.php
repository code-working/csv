<?php
namespace CodeWorking\CSV;

/**
 * The CSVWriter class.
 *
 * This is a wrapper of the CSV class that opens the resource in write-only mode by default.
 *
 * @author Joachim Beig
 *        
 */
class CsvWriter extends Csv
{

    /**
     * Create a new CSVWriter instance.
     *
     * @param string $file
     *            A file name or stream path
     * @param array $config
     */
    public function __construct($file, $config = [])
    {
        $config = $config + [
            'mode' => 'w'
        ];
        parent::__construct($file, $config);
    }
}