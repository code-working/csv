<?php
namespace CodeWorking\CSV;

/**
 * The CSVReader class.
 *
 * This is a wrapper of the CSV class that opens the resource in read-only mode by default.
 *
 * @author Joachim Beig
 *        
 */
class CsvReader extends Csv
{

    /**
     * Create a new CSVReader instance.
     *
     * @param string $file
     *            A file name or stream path
     * @param array $config
     */
    public function __construct($file, $config = [])
    {
        $config = $config + [
            'mode' => 'r'
        ];

        parent::__construct($file, $config);

        $this->loadHeader();
    }
}